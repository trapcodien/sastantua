/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sastantua.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/07/06 14:41:08 by garm              #+#    #+#             */
/*   Updated: 2014/07/06 22:35:19 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_put_something(char c, int len, int handle)
{
	int		i;

	i = 0;
	while (i < len)
	{
		if (handle && i == len - 2)
			ft_putchar('$');
		else
			ft_putchar(c);
		i++;
	}
}

int		get_door_size(int size)
{
	if (size <= 0)
		return (0);
	if (size == 1 || size == 2)
		return (1);
	return ((size % 2) ? (size) : (size - 1));
}

int		ft_display_line(int spaces, int stars, int stage, int size)
{
	int		door_size;

	door_size = get_door_size(size);
	if (stage == 1 && spaces < door_size)
	{
		stars -= door_size;
		stars /= 2;
		ft_put_something(' ', spaces, 0);
		ft_putchar('/');
		ft_put_something('*', stars, 0);
		if (spaces == door_size / 2 && size >= 5)
			ft_put_something('|', door_size, 1);
		else
			ft_put_something('|', door_size, 0);
		ft_put_something('*', stars, 0);
	}
	else
	{
		ft_put_something(' ', spaces, 0);
		ft_putchar('/');
		ft_put_something('*', stars, 0);
	}
	ft_putchar('\\');
	ft_putchar('\n');
	return (0);
}

int		recursive(int size, int stage, int spaces)
{
	static int		stars = 1;
	static int		counter = 0;
	int				limit;
	int				addsp;

	if (stage > size)
		return (0);
	limit = (size - stage) + 2;
	if (counter++ < limit)
		stars = recursive(size, stage, spaces + 1);
	else
	{
		counter = 0;
		if (++stage <= size)
		{
			if (((size - stage) + 1) <= 2)
				addsp = 3;
			else if (((size - stage) + 1) % 2)
				addsp = (limit / 2) + 2;
			else
				addsp = (limit / 2) + 1;
			stars = recursive(size, stage, spaces + addsp) + ((addsp * 2) - 2);
		}
	}
	return (ft_display_line(spaces, stars, stage, size) + stars + 2);
}

void	sastantua(int size)
{
	if (size > 188)
		return ;
	recursive(size, 1, 0);
}
